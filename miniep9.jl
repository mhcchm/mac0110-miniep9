function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    atual = M
    for i in 1:p-1
        atual = multiplica(atual, M)
    end
    return atual
end

function matrix_pot_by_squaring(M, p)
    dim = size(M)
    if p < 0
        ajuda = zeros(dim[1], dim[2])
        for i in 1:dim[1]
            for j in 1:dim[2]
                if M[i, j] == 0
                    continue
                end
                aux = 1/M[i, j]
                ajuda[i, j] = aux
            end
        end
        return matrix_pot_by_squaring(ajuda, -1*p)
    elseif p == 0
        identidade = zeros(dim[1], dim[2])
        for i in 1:dim[1]
            identidade[i, i] = 1
        end
        return identidade
    elseif p == 1
        return M
    elseif p % 2 == 0
        return matrix_pot_by_squaring(multiplica(M, M), p/2)
    else
        return multiplica(M, matrix_pot_by_squaring(multiplica(M, M), (p-1)/2))
    end
end

using Test
using LinearAlgebra
function Testa()
    ident = Matrix(LinearAlgebra.I, 30, 30)
    @test matrix_pot(ident, 10) == matrix_pot_by_squaring(ident, 10)
    @test matrix_pot(ident, 10) == matrix_pot_by_squaring(ident, -10)
    mat = ones(1, 1)
    @test matrix_pot(mat, 10) == matrix_pot_by_squaring(mat, 10)
    @test matrix_pot(mat, 1) == matrix_pot_by_squaring(mat, 1)
    @test matrix_pot([1 2 ; 3 4], 1) == matrix_pot_by_squaring([1 2 ; 3 4], 1)
    @test matrix_pot([1 2 ; 3 4], 2) == matrix_pot_by_squaring([1 2 ; 3 4], 2)
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
    @test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([1/4 1/8 0 1/4 ; 1/8 1/4 1/9 1/6 ; 1/9 1/6 1/4 0 ; 1/9 1/5 1/4 1/7], -7)
    println("top")
end
Testa()

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)

    @time matrix_pot(M, 10)
    @time matrix_pot_by_squaring(M, 10)
    @time matrix_pot_by_squaring(M, 30)
    @time matrix_pot_by_squaring(M, 100)
end

compare_times()